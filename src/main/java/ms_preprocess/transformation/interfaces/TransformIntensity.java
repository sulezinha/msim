/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms_preprocess.transformation.interfaces;

import ms_preprocess.transformation.methods.Transformations;

/**
 * This interface shows methods used to transform intensities on a spectrum.
 *
 * @author Sule
 */
public interface TransformIntensity {

    /**
     * This method transforms intensities on a spectrum     
     * 
     * @param tr an enum object of Transformations
     */
    public void transform(Transformations tr);

}
