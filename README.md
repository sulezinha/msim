# README #

Version 0.1 - The first version of MSim is about to see pairwise spectra on a score file.

Version 0.2 - The second version of MSim would be comparing selected MS2 spectra without any apriori score. In the meanwhile comparing score for that match.

Version 0.3 - The third version of MSim would be about integrated to spectrum library.

Note that this readme file is about the first version 

This is written on Java1.7. It is advisable not to use JDK1.8 due to broken swing rendering issue.

## Creating a project ##
### What files/folders are needed? ###
To create a visualization project, you need to have:

* spectra which are located in specA and specB
 (specA and specB show pairwise spectrum on mirrored spectra)

* a text file which holds information on which spectra are paired to visualize. 

### The steps to create a projects ###
1-Create two subfolders for each pair – spectraA and spectraB

2-	Create a text file with two columns (SpecA and SpecB)

3-	For each spectrum pairs, add spectra file name next to spectrum title with underscore such as spectraA_filename_with_extension_and_spectrum_title. For example, test1.mgf_130125.LC4.IT4.XX1 File:"", NativeID:"scan=3100"	

4-	Save that text file as a tab limited format  - spectrum_pairs_file.txt

5-	All you need to do is to write spectrum pairs from both subfolders on that text file before loading that spectrum_pairs_file.

6-	Click on run_gui.bat

7-	It may take a while to start up GUI. The default visualization is “bubble spectra”. After creating project, you may just see blue or bubbled spectra. Just select “mirrored spectra”. 

**Note:**  I have been always loading mgf files so far. In theory, all different formats can be uploaded to our tool but I suggest you use mgfs. 

**Note:**  Be careful if you create a file with Excel, you may add extra characters (“) to a name.


## Inspecting spectra ##

1.	You can select some part by using left click of mouse and drag from one location to another one.

2.	In some cases, one spectrum has much higher maximum intensities than the other one. In such situation, you cannot see some part of that spectrum. To resolve it, just make double right click on mirrored spectrum plot and both spectra would be fit into that plot. 

3.	It is also possible to remove some peaks by scrolling down “percentage of remaining peaks” part. It starts removing a peak with the minimum intensities and then removes the second peak with the 2nd lowest intensity, and so on, till an amount of remaining peaks percentage is actually decreased to that selected amount.
   	
## Dependencies ##
compomics-utilities 
